# Fullbar Select

# Instalação
Instale a diretiva vue-click-outside

```bash
npm install --save vue-click-outside
```

# Configurando

No componente ou screen que você for usar o select, importe ele e depois declare dentro de components

```javascript
import Select from '@/components/Select'

export default {
  components: {
    Select
  }
}
```

# Como Usar

```javascript
<Select
  @selecionou="selecionaFuncao"
  :Opcoes="[{ titulo: "teste", id: 1 }]"
  Placeholder="Selecione"
  Chave="titulo"
/>
```

Em @selecionou, coloque qual função o select deve chamar após uma seleção, exemplo:

```javascript
selecionaFuncao(e) {
  console.log(e)
}
```
Em Chave você deve colocar qual parte do objeto deve aparecer no select, no caso do exemplo que está como titulo, seria:

```json
[{ titulo: 'Aqui aparece', id: '1' }, { titulo: 'Outro elemento que aparece', id: '2' }]
```